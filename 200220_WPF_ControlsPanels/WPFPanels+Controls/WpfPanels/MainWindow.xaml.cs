﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfPanels
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void StackPanel_Click(object sender, RoutedEventArgs e)
        {
            StackPanel sp = new StackPanel();
            sp.Show();
        }

        private void WrapPanel_Click_1(object sender, RoutedEventArgs e)
        {
            Panels wp = new Panels();
            wp.Show();
        }

        private void DockPanel_Click_1(object sender, RoutedEventArgs e)
        {
            DockPanel dp = new DockPanel();
            dp.Show();
        }

        private void TabControl_Click_1(object sender, RoutedEventArgs e)
        {
            TabControl tc = new TabControl();
            tc.Show();
        }

        private void Grid_Click_1(object sender, RoutedEventArgs e)
        {
            Grid g = new Grid();
            g.Show();
        }

        private void Canvas_Click_1(object sender, RoutedEventArgs e)
        {
            Canvas c = new Canvas();
            c.Show();
        }

        private void TextBlock_Click(object sender, RoutedEventArgs e)
        {
            TextBlock tb = new TextBlock();
            tb.Show();
        }

        private void Label_Click(object sender, RoutedEventArgs e)
        {
            Labels l = new Labels();
            l.Show();
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            CheckBox cb = new CheckBox();
            cb.Show();
        }

        private void RadioButton_Click(object sender, RoutedEventArgs e)
        {
            RadioButton rb = new RadioButton();
            rb.Show();
        }

        private void TextBox_Click(object sender, RoutedEventArgs e)
        {
            Textbox tbox = new Textbox();
            tbox.Show();
        }
    }
}
