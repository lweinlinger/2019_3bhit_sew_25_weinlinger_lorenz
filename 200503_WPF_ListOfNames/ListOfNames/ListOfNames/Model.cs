﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;

namespace ListOfNames
{
    public class Model : INotifyPropertyChanged
    {
        public Model()
        {
            AddCommand = new AddNameCommand(this);
            CurrentPerson = new Person();
        }

        public AddNameCommand AddCommand { get; private set; }

        #region CurrentPerson

        public Person CurrentPerson
        {
            get { return mCurrentPerson; }
            set
            {
                if (value == mCurrentPerson)
                    return;

                mCurrentPerson = value;
                OnPropertyChanged(nameof(CurrentPerson));
                AddCommand.CurrentPerson = value;
            }
        }
        Person mCurrentPerson = new Person();

        #endregion

        public ObservableCollection<Person> AddedPersons { get; } = new ObservableCollection<Person>();

        public event PropertyChangedEventHandler PropertyChanged;

        void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
