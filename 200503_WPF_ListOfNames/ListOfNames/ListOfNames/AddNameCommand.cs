﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;

namespace ListOfNames
{
    public class AddNameCommand : ICommand
    {
        Model parent;

        public AddNameCommand(Model parent)
        {
            this.parent = parent;
            parent.PropertyChanged += delegate { CanExecuteChanged?.Invoke(this, EventArgs.Empty); };
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return CurrentPerson != null
                && !string.IsNullOrWhiteSpace(CurrentPerson.FirstName)
                && !string.IsNullOrWhiteSpace(CurrentPerson.LastName)
                ;
        }

        public void Execute(object parameter)
        {
            parent.AddedPersons.Add(parent.CurrentPerson);
            parent.CurrentPerson = new Person();
        }

        #region CurrentPerson

        public Person CurrentPerson
        {
            get { return mCurrentPerson; }
            set
            {
                if (value == mCurrentPerson)
                    return;

                PropertyChangedEventHandler handler = delegate { CanExecuteChanged?.Invoke(this, EventArgs.Empty); };
                if (CurrentPerson != null)
                    CurrentPerson.PropertyChanged -= handler;
                mCurrentPerson = value;
                if (CurrentPerson != null)
                    CurrentPerson.PropertyChanged += handler;
                handler(null, null);
            }
        }
        Person mCurrentPerson;

        #endregion
    }
}