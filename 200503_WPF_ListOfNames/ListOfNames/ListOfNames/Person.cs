﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ListOfNames
{
    public class Person : INotifyPropertyChanged
    {
        #region FirstName

        public string FirstName
        {
            get { return mFirstName; }
            set
            {
                if (value == mFirstName)
                    return;

                mFirstName = value;
                OnPropertyChanged(nameof(FirstName));
            }
        }
        string mFirstName;

        #endregion

        #region LastName

        public string LastName
        {
            get { return mLastName; }
            set
            {
                if (value == mLastName)
                    return;

                mLastName = value;
                OnPropertyChanged(nameof(LastName));
            }
        }
        string mLastName;

        #endregion

        public event PropertyChangedEventHandler PropertyChanged;

        void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
