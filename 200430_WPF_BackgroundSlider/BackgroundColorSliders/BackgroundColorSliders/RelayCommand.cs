﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BackgroundColorSliders
{
    class RelayCommand : ICommand
    {
        readonly Action<Boolean> _canExecute;
        readonly Action<object> _execute;

        public event EventHandler CanExecuteChanged;

        public RelayCommand(Action<object> execute) : this(execute, null) { }

        public RelayCommand(Action<object> execute, Action<Boolean> canExecute)
        {
            if (execute == null)
                throw new ArgumentNullException("execute");
            _execute = execute;
            _canExecute = canExecute;
        }
        

        public Boolean CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            object a = null;
            _execute(a);
        }
    }
}
