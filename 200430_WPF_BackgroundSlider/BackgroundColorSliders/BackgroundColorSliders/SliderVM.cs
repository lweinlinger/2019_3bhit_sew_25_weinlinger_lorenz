﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows.Input;

namespace BackgroundColorSliders
{
    class SliderVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private int redValue;
        public int RedValue
        {
            get 
            { 
                return redValue;
            }   
            set 
            {
                redValue = value;
                OnPropertyChanged("RedValue");
            }
            
        }

        private int greenValue;
        public int GreenValue
        {
            get
            {
                return greenValue;
            }
            set
            {
                greenValue = value;
                OnPropertyChanged("GreenValue");
            }

        }

        private int blueValue;
        public int BlueValue
        {
            get
            {
                return blueValue;
            }
            set
            {
                blueValue = value;
                OnPropertyChanged("BlueValue");
            }

        }

        public RelayCommand RandomColour
        {
            get
            {
                return new RelayCommand(o => NewRandomCommand());
            }
        }
        private void NewRandomCommand()
        {
            Random rnd = new Random();
            RedValue = rnd.Next(1, 255);
            GreenValue = rnd.Next(1, 255);
            BlueValue = rnd.Next(1, 255);
        }

        private void OnPropertyChanged(string property)
        {
           
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
                
            }
        }

    }
}
