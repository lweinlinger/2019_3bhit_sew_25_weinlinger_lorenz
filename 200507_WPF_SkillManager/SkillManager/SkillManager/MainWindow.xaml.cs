﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SkillManager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            List<Skills> skills = new List<Skills>();
            skills.Add(new Skills("C#", 80));
            skills.Add(new Skills("Java", 50));
            skills.Add(new Skills("Python", 20));
            skills.Add(new Skills("Angular", 20));
            skills.Add(new Skills("Entity Framework", 60));
            skills.Add(new Skills("Ember", 0));

            var skillVM = DataContext as SkillViewModel;
            skillVM.AddSkills(skills);
            
        }
    }
}
