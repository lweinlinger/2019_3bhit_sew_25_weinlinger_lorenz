﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace SkillManager
{
    class Skills : INotifyPropertyChanged
    {
        private string name;
        private int percent;

        public event PropertyChangedEventHandler PropertyChanged;

        public Skills() { }

        public string Name
        {
            get { return name; }
            set { name = value; OnPropertyChanged("Name"); }
        }
        public int Percent
        {
            get { return percent; }
            set
            {
                if (value >= 0 && value <= 100)
                    percent = value;
                OnPropertyChanged("Percent");
            }
        }

        void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public Skills(string _name, int _percent)
        {
            Name = _name;
            Percent = _percent;
        }
    }
}
