﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SkillManager
{
    class RemovePercentageCommand : ICommand
    {
        private SkillViewModel parent;
        public event EventHandler CanExecuteChanged;

        public RemovePercentageCommand(SkillViewModel skillViewModel)
        {
            parent = skillViewModel;
        }
        public bool CanExecute(object parameter)
        {
            return parent.SelectedSkill != null;
        }

        public void Execute(object parameter)
        {
            parent.RemovePercentage();
        }
    }
}
