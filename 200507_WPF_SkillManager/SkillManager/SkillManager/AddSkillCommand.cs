﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SkillManager
{
    class AddSkillCommand : ICommand
    {
        private SkillViewModel parent;
        public event EventHandler CanExecuteChanged;

        public AddSkillCommand(SkillViewModel skillViewModel)
        {
            parent = skillViewModel;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            parent.AddSkill();
        }
    }
}
