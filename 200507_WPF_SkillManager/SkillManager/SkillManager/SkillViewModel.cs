﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;

namespace SkillManager
{
    class SkillViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;       
        private Skills selectedSkill = null;
        private Skills skill;

        public ObservableCollection<Skills> _skills;

        public ICommand AddSkillCommand { get; private set; }
        public ICommand AddPercentageCommand { get; private set; }
        public ICommand RemovePercentageCommand { get; private set; }


        public ObservableCollection<Skills> Skills
        {
            get
            {
                if (_skills == null)
                    _skills = new ObservableCollection<Skills>();
                return _skills;
            }
            set
            {
                _skills = value;
                OnPropertyChanged("Skills");
            }
        }

        public Skills SelectedSkill
        {
            get
            {
                if (selectedSkill == null)
                    selectedSkill = new Skills();
                return selectedSkill;
            }
            set 
            {
                selectedSkill = value;
                OnPropertyChanged("SelectedSkill");
            }
        }


        public Skills Skill
        {
            get 
            {
                if (skill == null)                
                    skill = new Skills();                
                return skill;
            }
            set
            {
                skill = value;
                OnPropertyChanged("Skill");
            }
        }

        public SkillViewModel()
        {
            AddSkillCommand = new AddSkillCommand(this);
            AddPercentageCommand = new AddPercentageCommand(this);
            RemovePercentageCommand = new RemovePercentageCommand(this);
        }

        public SkillViewModel(List<Skills> _skills)
        {
            AddSkills(_skills);
        }

        public void AddPercentage()
        {
            SelectedSkill.Percent += 10;
        }

        public void RemovePercentage()
        {
            SelectedSkill.Percent -= 10;
        }

        public void AddSkill()
        {
            Skills.Add(new Skills(Skill.Name, 0));
            Skill.Name = "";
        }



        private void OnPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }

        public void AddSkills(List<Skills> _skills)
        {
            foreach(Skills item in _skills)
            {
                Skills.Add(new Skills(item.Name, item.Percent));
            }
        }
    }
}
