﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MulticastDelegateEmployee
{
    class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime PositionChangeDate { get; set; }
        public decimal Salary { get; set; }
    }
}
