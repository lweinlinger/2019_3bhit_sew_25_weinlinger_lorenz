﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MulticastDelegateEmployee
{
    class Company
    {
        public List<Employee> Employees { get; }

        public delegate void CustomDel(Employee employee);

        CustomDel SalaryRaiseDel, PositionDateDel, EmployeePromoteMulticastDelegate;

        public Company()
        {
            Employees = new List<Employee>()
            {
                new Employee(){Id = 1, Name = "Max", Salary = 10},
                new Employee() { Id = 2, Name = "Kurt", Salary = 60 },
                new Employee() { Id = 3, Name = "Elisabeth", Salary = 30 },                
                new Employee() { Id = 5, Name = "Sabine", Salary = 50 }
            };

            SalaryRaiseDel = new CustomDel(EmployeeSalaryRaise);
            PositionDateDel = new CustomDel(PositionDateUpdate);
            EmployeePromoteMulticastDelegate = SalaryRaiseDel + PositionDateDel;

        }

        public void PromoteEmployees()
        {
            foreach(Employee emp in Employees)
            {
                EmployeePromoteMulticastDelegate(emp);

                Console.WriteLine($"{emp.Id} - {emp.Name}");
                Console.Write("LastUpdate " + emp.PositionChangeDate.ToShortDateString());
                Console.WriteLine(" Salary = " + emp.Salary);
            }
        }

        private void PositionDateUpdate(Employee employee)
        {
            employee.PositionChangeDate = DateTime.Now;
        }

        private void EmployeeSalaryRaise(Employee employee)
        {
            employee.Salary += employee.Salary + employee.Salary * (decimal)0.1;
        }
    }
}
