﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegateNumberChanger
{
    class Program
    {
        static void Main(string[] args)
        {
            NumberChanger nc;
            NumberChanger nc1 = new NumberChanger(DelegateStuff.AddNum);
            NumberChanger nc2 = new NumberChanger(DelegateStuff.MultNum);



            nc1(25);
            Console.WriteLine("Value of Num: " + DelegateStuff.getNum());
            nc2(5);
            Console.WriteLine("Value of Num: " + DelegateStuff.getNum());

        }
    }
}
