﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegateBookStore
{
    class Program
    {
        static void PrintTitle(Book b)
        {
            Console.WriteLine($"    {b.Title}");
        }

        static void AddBooks(BookDB bookDB)
        {
            bookDB.AddBook("Title #1", "Author #1", 19.95m, true);
            bookDB.AddBook("Title #2", "Author #2", 39.95m, true);
            bookDB.AddBook("Title #3", "Author #3", 29.95m, false);
            bookDB.AddBook("Title #4", "Author #4", 49.95m, true);
        }
        static void Main(string[] args)
        {
            BookDB bookDB = new BookDB();

            AddBooks(bookDB);

            Console.WriteLine("Paperback Book Titles:");
            bookDB.ProcessPaperbackBooks(PrintTitle);

            PriceTotaller pt = new PriceTotaller();
            bookDB.ProcessPaperbackBooks(pt.AddBookToTotal);

            Console.WriteLine("Average Paperback Book Price: ${0:#.##}", pt.AveragePrice());
        }
    }
}
