﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegateBookStore
{
    class BookDB
    {
        List<Book> list = new List<Book>();

        public void AddBook(string title, string author, decimal price, bool paperback)
        {
            list.Add(new Book(title, author, price, paperback));
        }


        public void ProcessPaperbackBooks(ProcessBookDelegate processBook)
        {
            foreach(Book b in list)
            {
                if (b.Paperback)
                    processBook(b);
            }
        }
    }
}
