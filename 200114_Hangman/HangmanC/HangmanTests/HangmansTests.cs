using Microsoft.VisualStudio.TestTools.UnitTesting;
using Hangman;

namespace HangmanTests
{
    [TestClass]
    public class HangmanTest
    {
        [TestMethod]
        public void ReturnInfoTest()
        {
            HangmanLogic hml = HangmanLogic.Instance;
            string hangman = "";
            string word;
            hml.Initialize();

            word = hml.ReturnInfo(ref hangman);

            Assert.AreEqual(hangman, "");
        }

    }
}
