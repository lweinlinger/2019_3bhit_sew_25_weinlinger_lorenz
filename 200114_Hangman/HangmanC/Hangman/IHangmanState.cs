﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman
{
    interface IHangmanState
    {
        string MiniLogic(ref int lives, char guess, string gw, string word);
    }

}
