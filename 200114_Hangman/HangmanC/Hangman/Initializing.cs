﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Hangman
{
    class Initializing : IHangmanState
    {
        public string MiniLogic(ref int lives, char guess, string gw, string word)
        {
            return GetWord();
        }

        public string GetWord()
        {
            Random rnd = new Random();
            List<string> words = new List<string>();
            try
            {
                words.AddRange(File.ReadAllLines(@"WordList.csv"));
            }
            catch
            {
                return "Hangman";
            }
            return words[rnd.Next(49)];
        }
    }
}
