﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman
{
    class Program
    {
        static void Main(string[] args)
        {
            HangmanLogic hml = HangmanLogic.Instance;
            string hangman = "";
            string word;
            hml.Initialize();

            do
            {
                Console.Clear();

                word = hml.ReturnInfo(ref hangman);

                Console.WriteLine(hangman);
                Console.WriteLine(word);

                Console.WriteLine("Geben sie einen Buchstaben an:");
                string guessStr = Console.ReadLine();

                hml.MakeGuess(Convert.ToChar(guessStr));
                word = hml.ReturnInfo(ref hangman);
            } while (word != "Du hast Gewonnen!" && word != "Du hast verloren!");

            Console.Clear();
            Console.WriteLine(hangman);
            Console.WriteLine(word);            
        }
    }
}
