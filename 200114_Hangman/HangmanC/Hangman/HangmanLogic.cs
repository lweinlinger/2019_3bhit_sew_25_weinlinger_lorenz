﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman
{
    public sealed class HangmanLogic
    {

        private static readonly HangmanLogic instance = new HangmanLogic();
        
        static HangmanLogic()
        {
        }

        private HangmanLogic()
        {
        }

        public static HangmanLogic Instance
        {
            get
            {
                return instance;
            }
        }

        string word = ""; //Unterstrich Wort
        string hangman; //Hangman Zeichnung
        int lives; //Leben des Spielers
        char guess; //Buchstabe der geraten wurde
        string gw; //Originales Wort 
        IHangmanState hms = new Initializing();
        

        public void Initialize()
        {
            lives = 9;
            gw = hms.MiniLogic(ref lives, guess, "", "");
            
            word = word.PadRight(gw.Length,'_');
            hms = new Playing();
            
            
        }

        public void MakeGuess(char guess)
        {

            string result = hms.MiniLogic(ref lives, guess, gw, word);

            word = result;

            switch (result)
            {
                case "SameChar":
                    hms = new SameChar();
                    hms.MiniLogic(ref lives, 'a', "", "");
                    break;

                case "Win":
                    hms = new Won();
                    word = hms.MiniLogic(ref lives, 'a', "", "");
                    break;

                case "Loss":
                    hms = new Lost();
                    word = hms.MiniLogic(ref lives, 'a', "", "");
                    break;
            }
                

                
        }

        public string ReturnInfo(ref string Hangman)
        {
            Hangman = HangmanFactory.GetHangman(lives);
            return word;
        }
    }
}
