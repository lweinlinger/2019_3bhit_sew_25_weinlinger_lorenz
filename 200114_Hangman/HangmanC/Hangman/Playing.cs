﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman
{
    class Playing : IHangmanState
    {
        public string Word { get; set; }
        int lives;
        char[] used = new char[26];
        int usedIndex;
 

        public string MiniLogic(ref int _lives, char guess, string gw, string word)
        {
            lives = _lives;

            word = Checkguess(guess, gw, word);

            if (word == "SameChar")
                return "SameChar";

            word = CheckWinLose(gw, word);

            _lives = lives;
            return word;
        }
        
        public string Checkguess(char guess, string gw, string word)
        {
            int wordIndex = 0;
            bool changed = false;

            foreach(char letter in used)
            {
                if (letter == guess)
                    return "SameChar";

            }
            usedIndex++;
            used[usedIndex] = guess;

            foreach(char letter in gw)
            {
                if (letter == guess)
                {
                    word = word.Remove(wordIndex,1);
                    word = word.Insert(wordIndex,guess.ToString());
                    changed = true;
                }
                wordIndex++;
            }

            if (changed == false)
                lives--;
            
            return word;
        }

        public string CheckWinLose(string gw, string word)
        {
            if (lives <= 0)
                return "Loss";
            else if (word == gw)
                return "Win";
            else
                return word;
        }

    }
}
