﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman
{
    static class HangmanFactory
    {
        static public string GetHangman(int lives)
        {
            switch (lives)
            {
                case 9:
                    return "";

                case 8:
                    return "\n  ---------\n /         \\\n/           \\\n";
                    
                case 7:
                    return "\n   |\n   |  \n   |  \n   |  \n   |  \n   |\n  ---------\n /         \\\n/           \\\n";

                case 6:
                    return "\n   _________\n   |\n   |  \n   |  \n   |  \n   |  \n   |\n  ---------\n /         \\\n/           \\\n";
                    
                case 5:
                    return "\n   _________\n   |/\n   |  \n   |  \n   |  \n   |  \n   |\n  ---------\n /         \\\n/           \\\n";

                case 4:
                    return "\n   _________\n   |/  |\n   |   O\n   |  \n   |  \n   |  \n   |\n  ---------\n /         \\\n/           \\\n";

                case 3:
                    return "\n   _________\n   |/  |\n   |   O\n   |   | \n   |   |\n   |  \n   |\n  ---------\n /         \\\n/           \\\n";

                case 2:
                    return "\n   _________\n   |/  |\n   |   O\n   |  (| \n   |   |\n   |  \n   |\n  ---------\n /         \\\n/           \\\n";

                case 1:
                    return "\n   _________\n   |/  |\n   |   O\n   |  (|) \n   |   |\n   |  \n   |\n  ---------\n /         \\\n/           \\\n";

                default:
                    return "\n   _________\n   |/  |\n   |   O\n   |  (|) \n   |   |\n   |   (\\  \n   |\n  ---------\n /         \\\n/           \\\n";

            }
        }
    }
}
