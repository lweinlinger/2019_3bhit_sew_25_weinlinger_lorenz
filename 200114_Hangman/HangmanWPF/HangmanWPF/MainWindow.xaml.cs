﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Hangman;

namespace HangmanWPF
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        HangmanLogic hml = HangmanLogic.Instance;
        string word;
        string hangman;
        string used;

        public MainWindow()
        {
            InitializeComponent();
            hml.Initialize();
            word = hml.ReturnInfo(ref hangman);

            HangmanLabel.Content = hangman;
            WordBox.Text = word;
        }

        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            

            string guessStr = GuessBox.Text;
            GuessBox.Clear();

            BlockUsed.Text = BlockUsed.Text + guessStr;

            hml.MakeGuess(Convert.ToChar(guessStr));

            word = hml.ReturnInfo(ref hangman);

            HangmanLabel.Content = hangman;
            WordBox.Text = word;



            word = hml.ReturnInfo(ref hangman);
            if (word == "Du hast Gewonnen!" || word == "Du hast verloren!")
            {
                WordBox.Text = word;
                btnSubmit.Opacity = 0;
                GuessBox.Opacity = 0;
            }
        }
    }
}
