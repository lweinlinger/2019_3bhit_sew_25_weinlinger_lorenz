﻿namespace HangmanWF
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.HangmanLabel = new System.Windows.Forms.Label();
            this.GuessBox = new System.Windows.Forms.TextBox();
            this.WordLabel = new System.Windows.Forms.Label();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // HangmanLabel
            // 
            this.HangmanLabel.AutoSize = true;
            this.HangmanLabel.Location = new System.Drawing.Point(422, 168);
            this.HangmanLabel.Name = "HangmanLabel";
            this.HangmanLabel.Size = new System.Drawing.Size(79, 29);
            this.HangmanLabel.TabIndex = 0;
            this.HangmanLabel.Text = "label1";
            // 
            // GuessBox
            // 
            this.GuessBox.Location = new System.Drawing.Point(153, 239);
            this.GuessBox.Name = "GuessBox";
            this.GuessBox.Size = new System.Drawing.Size(100, 35);
            this.GuessBox.TabIndex = 1;
            this.GuessBox.TextChanged += new System.EventHandler(this.GuessBox_TextChanged);
            // 
            // WordLabel
            // 
            this.WordLabel.AutoSize = true;
            this.WordLabel.Location = new System.Drawing.Point(148, 168);
            this.WordLabel.Name = "WordLabel";
            this.WordLabel.Size = new System.Drawing.Size(79, 29);
            this.WordLabel.TabIndex = 2;
            this.WordLabel.Text = "label2";
            // 
            // btnSubmit
            // 
            this.btnSubmit.Location = new System.Drawing.Point(153, 312);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(134, 58);
            this.btnSubmit.TabIndex = 3;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(14F, 29F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1325, 755);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.WordLabel);
            this.Controls.Add(this.GuessBox);
            this.Controls.Add(this.HangmanLabel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label HangmanLabel;
        private System.Windows.Forms.TextBox GuessBox;
        private System.Windows.Forms.Label WordLabel;
        private System.Windows.Forms.Button btnSubmit;
    }
}

