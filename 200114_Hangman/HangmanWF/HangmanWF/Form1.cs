﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Hangman;

namespace HangmanWF
{
    public partial class Form1 : Form
    {
        HangmanLogic hml = HangmanLogic.Instance;
        string word;
        string hangman;


        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            hml.Initialize();
            word = hml.ReturnInfo(ref hangman);

            HangmanLabel.Text = hangman;
            WordLabel.Text = word;
        }

        private void GuessBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            
            string guessStr = GuessBox.Text;
            GuessBox.Clear();

            hml.MakeGuess(Convert.ToChar(guessStr));

            word = hml.ReturnInfo(ref hangman);

            HangmanLabel.Text = hangman;
            WordLabel.Text = word;

            

            word = hml.ReturnInfo(ref hangman);
            if (word == "Du hast Gewonnen!" || word == "Du hast verloren!")
            {
                WordLabel.Text = word;
                btnSubmit.Hide();
                GuessBox.Hide();
            }
                
        }
    }
}
