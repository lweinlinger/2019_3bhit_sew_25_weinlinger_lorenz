﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.Windows;

namespace PatienList
{
    class PatientDetailViewModel : INotifyPropertyChanged
    {
        private readonly Patient domObject;
        private readonly PatientManager patientManager;
        private readonly ObservableCollection<Patient> _patients;
        private readonly ICommand _addPatientCmd;
        private readonly ICommand _deletePatientCmd;
        private readonly ICommand _searchPatientCmd;

        public PatientDetailViewModel()
        {
            domObject = new Patient();
            patientManager = new PatientManager();

            _patients = new ObservableCollection<Patient>();
            _addPatientCmd = new RelayCommand(Add, CanAdd);
            _deletePatientCmd = new RelayCommand(Delete, CanDelete);
            _searchPatientCmd = new RelayCommand(Search, CanSearch);
        }


        public int Id
        {
            get { return domObject.Id; }
            set
            {
                domObject.Id = value;
                OnPropertyChanged("Id");
            }
        }
        public string Name
        {
            get { return domObject.Name; }
            set
            {
                domObject.Name = value;
                OnPropertyChanged("Name");
            }
        }

        public Int64 MobileNumber
        {
            get { return domObject.MobileNumber; }
            set
            {
                domObject.MobileNumber = value;
                OnPropertyChanged("MobileNumber");
            }
        }
        public ObservableCollection<Patient> Patients { get { return _patients; } }

        public Patient SelectedPatient
        {
            set
            {
                Id = value.Id;
                MobileNumber = value.MobileNumber;
                Name = value.Name;
            }
        }

        public ICommand AddPatientCmd { get { return _addPatientCmd; } }
        public ICommand DeletePatientCmd { get { return _deletePatientCmd; } }
        public ICommand SearchPatientCmd { get { return _searchPatientCmd; } }


        public bool CanAdd(object obj)
        {
            if (Name != string.Empty && MobileNumber != 0)
                return true;
            return false;
        }
        public void Add(object obj)
        {
            var patient = new Patient { Id = Id, Name = Name, MobileNumber = MobileNumber };

            if (patientManager.Add(patient))
            {
                Patients.Add(patient);
                ResetPatient();
                MessageBox.Show("Patient Add Successful !");
            }
            else
                MessageBox.Show("Patient with this ID already exists !");
        }


        private bool CanDelete(object obj)
        {
            
            if (Patients.Count > 0)
                return true;
            return false;
        }
        private void Delete(object obj)
        {
            
            if (!patientManager.Remove(Id))
                MessageBox.Show("Patient with this ID does not exist !");
            else
            {
              
                Patients.RemoveAt(GetIndex(Id));
                ResetPatient();
                MessageBox.Show("Patient Remove Successful !");
            }
        }

        private bool CanSearch(object obj)
        {
            
            if (Patients.Count > 0)
                return true;
            return false;
        }

        private void Search(object obj)
        {
            Patient patient = patientManager.Search(Id);

            if (patient == null)
                MessageBox.Show("Patient with this ID does not exist !");
            else
            {
                
                Id = patient.Id;
                Name = patient.Name;
                MobileNumber = patient.MobileNumber;
            }
        }
        private void ResetPatient()
        {
            Id = 0;
            Name = string.Empty;
            MobileNumber = 0;
        }
        private int GetIndex(int Id)
        {
            for (int i = 0; i < Patients.Count; i++)
                if (Patients[i].Id == Id)
                    return i;
            return -1;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
        