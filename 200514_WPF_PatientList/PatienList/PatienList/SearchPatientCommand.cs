﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows;

namespace PatienList
{
    public class SearchPatientCommand : ICommand
    {

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            Patient patientDetail = parameter as Patient;

            Patient patient = new PatientManager().Search(patientDetail.Id);

            if (patient == null)
                MessageBox.Show("Patient with this ID does not exist !");
            else
            {
                patientDetail.Id = patient.Id;
                patientDetail.Name = patient.Name;
            }
        }     
    }
}
