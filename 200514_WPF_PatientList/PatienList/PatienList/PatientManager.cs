﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace PatienList
{
    class PatientManager
    {
        readonly PatientRepository patientRepository;

        public PatientManager()
        {
            patientRepository = new PatientRepository();
        }

        public bool Add(Patient patient)
        {
            if (patientRepository.Search(patient.Id) == null)
            {
                patientRepository.Add(patient);
                return true;
            }
            return false;
        }

        public bool Remove(int id)
        {
            Patient patient = patientRepository.Search(id);
            if (patient != null)
            {
                patientRepository.Remove(patient);
                return true;
            }
            return false;
        }

        public Patient Search(int id)
        {
            return patientRepository.Search(id);
        }
    }
}