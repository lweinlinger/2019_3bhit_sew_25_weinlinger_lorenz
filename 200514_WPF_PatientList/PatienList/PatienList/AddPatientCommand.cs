﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows;

namespace PatienList
{
    public class AddPatientCommand : ICommand
    {
 
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            PatientManager patientManager = new PatientManager();
            Patient patientDetail = parameter as Patient;

            if (patientManager.Add(new Patient { Id = patientDetail.Id, Name = patientDetail.Name }))
                MessageBox.Show("Patient Add Successful !");
            else
                MessageBox.Show("Patient with this ID already exists !");
        }

   
    }
}


