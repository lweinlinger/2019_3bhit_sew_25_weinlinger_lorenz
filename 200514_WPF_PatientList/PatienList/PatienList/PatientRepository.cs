﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PatienList
{
        public class PatientRepository
        {
            
            private static List<Patient> patients = new List<Patient>();

            
            internal void Add(Patient patient)
            {
                patients.Add(patient);
            }

          
            internal void Remove(Patient patient)
            {
                patients.Remove(patient);
            }


            internal Patient Search(int id)
            {               
                int index = GetIndex(id);               
                if (index > -1)
                    return patients[index];
                return null;
            }

            private int GetIndex(int id)
            {
                int index = -1;               
                if (patients.Count > 0)
                {                
                    for (int i = 0; i < patients.Count; i++)
                    {                       
                        if (patients[i].Id == id)
                        {
                            index = i;
                            break;
                        }
                    }
                }
                return index;
            }
        }
    }
