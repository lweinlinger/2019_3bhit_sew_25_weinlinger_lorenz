﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace TimeToolLogic
{
    class TimeToolLogic : ITTLogic
    {
        public List<Tasks> Q1List { get; private set; }
        public List<Tasks> Q2List { get; private set; }
        public List<Tasks> Q3List { get; private set; }
        public List<Tasks> Q4List { get; private set; }

        public void AddTask(string name, string desc, string urgency, string imp, State state)
        {
            if(urgency == "Urgent")
            {
                if(imp == "Important")               
                    Q1List.Add(new Tasks(name, desc, urgency, imp, state));                
                else
                    Q3List.Add(new Tasks(name, desc, urgency, imp, state));
            }
            else
            {
                if (imp == "Important")
                    Q2List.Add(new Tasks(name, desc, urgency, imp, state));
                else
                    Q4List.Add(new Tasks(name, desc, urgency, imp, state));
            }
        }

        public void ChangeState(Tasks task, State state)
        {
            task.TaskState = state;
        }

        public void SaveTasks()
        {
            WriteListToFile(Q1List, "Q1.csv");
            WriteListToFile(Q2List, "Q2.csv");
            WriteListToFile(Q3List, "Q3.csv");
            WriteListToFile(Q4List, "Q4.csv");
        }

        public void WriteListToFile(List<Tasks> list,string path)
        {
            foreach(Tasks item in list)
            {
                File.AppendAllText(path, item.ToString());
            }
        }

        public void LoadTasks()
        {
            WriteFileToList(Q1List, "Q1.csv");
            WriteFileToList(Q2List, "Q2.csv");
            WriteFileToList(Q3List, "Q3.csv");
            WriteFileToList(Q4List, "Q4.csv");
        }
        public void WriteFileToList(List<Tasks> list, string path)
        {
            string[] lines = File.ReadAllLines(path);
            foreach(string line in lines)
            {
                string[] items = line.Split(';');
                list.Add(new Tasks(items[0], items[1], items[2], items[3], (State)Enum.Parse(typeof(State), items[4])));
            }
        }
    }
}
