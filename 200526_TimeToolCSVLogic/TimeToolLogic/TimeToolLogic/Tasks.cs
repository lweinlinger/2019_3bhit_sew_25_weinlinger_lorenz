﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TimeToolLogic
{
    public enum State
    {
        Open,
        InProgress, 
        Done
    }
    public class Tasks
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Urgency { get; set; }
        public string Importance { get; set; }
        public State TaskState { get; set; }

        public Tasks(string name, string desc, string urg, string imp, State state)
        {
            Name = name;
            Description = desc;
            Urgency = urg;
            Importance = imp;
            TaskState = state;
        }

        public override string ToString()
        {
            return $"{Name};{Description};{Urgency};{Importance};{TaskState}";
        }
    }
}
