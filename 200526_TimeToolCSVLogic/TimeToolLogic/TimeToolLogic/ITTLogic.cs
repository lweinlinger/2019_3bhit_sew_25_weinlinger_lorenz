﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TimeToolLogic
{
    interface ITTLogic
    {
        void AddTask(string name, string desc, string urgency, string imp, State state);
        void ChangeState(Tasks task, State state);
        void SaveTasks();
        void LoadTasks();
    }
}
