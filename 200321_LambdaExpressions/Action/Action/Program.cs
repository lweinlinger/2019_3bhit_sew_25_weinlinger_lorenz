﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Action
{
    class Program
    {
        public delegate void Print(int val);

        static void ConsolePrint(int i)
        {
            Console.WriteLine(i);
        }

        static void Main(string[] args)
        {
            Print prnt = ConsolePrint;
            prnt(10);

            Action<int> printActionDel = ConsolePrint;
            printActionDel(11);

            Action<int> printActionDel2 = delegate (int i)
            {
                Console.WriteLine(i);
            };
            printActionDel2(12);

            Action<int> printActionDel3 = i => Console.WriteLine(i);
            printActionDel3(13);
        }
    }
}
