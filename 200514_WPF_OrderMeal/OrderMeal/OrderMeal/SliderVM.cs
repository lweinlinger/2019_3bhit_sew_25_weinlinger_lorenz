﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace OrderMeal
{
    public enum EWeekdays
    {
        Monday,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday,
        Sunday
    }
    class SliderVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private EWeekdays eweekday;

        public SliderVM()
        {
            eweekday = EWeekdays.Friday;
        }

        public EWeekdays EWeekday
        {
            get
            {
                return eweekday;
            }
            set
            {
                eweekday = value;
                OnPropertyChanged("EWeekday");

            }
        }

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
