﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
namespace JumpingShapes
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        DispatcherTimer timer = new DispatcherTimer();
        public MainWindow()
        {
            InitializeComponent();

            timer.Tick += Physics;
            timer.Interval = TimeSpan.FromSeconds(0.05);

        }

        private void ButtonStartStop_Click(object sender, RoutedEventArgs e)
        {
            timer.IsEnabled = !timer.IsEnabled;
        }

        private void radioButtonRed_Click(object sender, RoutedEventArgs e)
        {
            Ball.Fill = Brushes.Red;
            Rectangle.Fill = Brushes.Red;
            Triangle.Fill = Brushes.Red;
        }
        private void radioButtonGreen_Click(object sender, RoutedEventArgs e)
        {
            Ball.Fill = Brushes.Green;
            Rectangle.Fill = Brushes.Green;
            Triangle.Fill = Brushes.Green;
        }
        private void radioButtonBlue_Click(object sender, RoutedEventArgs e)
        {
            Ball.Fill = Brushes.Blue;
            Rectangle.Fill = Brushes.Blue;
            Triangle.Fill = Brushes.Blue;
        }

        bool ballGoingRight = true;
        bool ballGoingDown = true;

        bool triGoingRight = true;
        bool triGoingDown = true;

        bool rectGoingRight = true;
        bool rectGoingDown = true;

        private void Physics(object sender, EventArgs e)
        {
            double speed = 3.0;
            if (CheckBoxFast.IsChecked.Value)
                speed = 10.0;

            double triSpeed = speed;
            double rectSpeed = speed;
            double ballSpeed = speed;

            ballGoingRight = MoveShapeLeftRight(Ball,ballSpeed, ballGoingRight);
            ballGoingDown = MoveShapeUpDown(Ball, ballSpeed, ballGoingDown);
            triGoingRight = MoveShapeLeftRight(Triangle, triSpeed, triGoingRight);
            triGoingDown = MoveShapeUpDown(Triangle, triSpeed, triGoingDown);
            rectGoingRight = MoveShapeLeftRight(Rectangle, rectSpeed, rectGoingRight);
            rectGoingDown = MoveShapeUpDown(Rectangle, rectSpeed, rectGoingDown);
        }

        private bool MoveShapeUpDown(Shape someShape, double speed, bool GoingDown)
        {
            double y = Canvas.GetTop(someShape);
            if (GoingDown)
                y += speed;
            else
                y -= speed;

            if (y + someShape.Height > TheCanvas.ActualHeight)
            {
                GoingDown = false;
                y = TheCanvas.ActualHeight - someShape.Height;
                System.Media.SystemSounds.Asterisk.Play();
            }
            else if (y < 0.0)
            {
                GoingDown = true;
                y = 0.0;
                System.Media.SystemSounds.Beep.Play();
            }

            Canvas.SetTop(someShape, y);
            return GoingDown;
        }

        private bool MoveShapeLeftRight(Shape someShape, double speed, bool GoingRight)
        {
            double x = Canvas.GetLeft(someShape);
            if (GoingRight)
                x += speed;
            else
                x -= speed;

            if (x + someShape.Width > TheCanvas.ActualWidth)
            {
                GoingRight = false;
                x = TheCanvas.ActualWidth - Ball.Width;
                System.Media.SystemSounds.Asterisk.Play();
            }
            else if (x < 0.0)
            {
                GoingRight = true;
                x = 0.0;
                System.Media.SystemSounds.Beep.Play();
            }

            Canvas.SetLeft(someShape, x);
            return GoingRight;
        }

        int score = 0;
        private void Ball_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (timer.IsEnabled)
            {
                score++;
                labelScore.Content = score;
            }
        }

    }
}
