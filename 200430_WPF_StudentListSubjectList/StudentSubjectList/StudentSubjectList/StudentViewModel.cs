﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using System.IO;

namespace StudentSubjectList
{
    class StudentViewModel : INotifyPropertyChanged
    {
        private Student _student;
        private string path = "students.csv";
        private ObservableCollection<Student> _students;
        public event PropertyChangedEventHandler PropertyChanged;

        public ICommand AddCommand { get; private set; }
        public ICommand LoadCommand { get; private set; }

        public StudentViewModel()
        {
            AddCommand = new AddCommand(this);
            LoadCommand = new LoadCommand(this);
        }

        public Student Student
        {
            get
            {
                if (_student == null)
                    _student = new Student();
                return _student;
            }
            set
            {
                _student = value;
                OnPropertyChanged("Student");
            }
        }

        public EClass Class
        {
            get
            {
                return Student.Clazz;
            }
            set
            {
                Student.Clazz = value;
                OnPropertyChanged("Class");
            }
        }

        public string Firstname
        {
            get
            {
                return Student.Firstname;
            }
            set
            {
                Student.Firstname = value;
                OnPropertyChanged("Firstname");
            }
        }
        public string Lastname
        {
            get
            {
                return Student.Lastname;
            }
            set
            {
                Student.Lastname = value;
                OnPropertyChanged("Lastname");
            }
        }
        public int Age
        {
            get
            {
                return Student.Age;
            }
            set
            {
                Student.Age = value;
                OnPropertyChanged("Age");
            }
        }

        public ObservableCollection<Student> Students
        {
            get
            {
                return _students;
            }
            set
            {
                _students = value;
                OnPropertyChanged("Students");
            }
        }
        public IEnumerable<EClass> AllClassTypes
        {
            get
            {
                return Enum.GetValues(typeof(EClass)).Cast<EClass>();
            }
        }
        
        private void OnPropertyChanged(string property)
        {
            if(PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        public void Save(Student s)
        {
            StreamWriter sw = new StreamWriter(path, true);
            sw.WriteLine(s.ToCSV());
            sw.Close();
        }
        public void LoadFromCSV()
        {
            ObservableCollection<Student> students = new ObservableCollection<Student>();
            if (File.Exists(path))
            {
                StreamReader sr = new StreamReader(path);
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    string[] splittedLine = line.Split(';');
                    students.Add(new Student(splittedLine[0], splittedLine[1], Convert.ToInt32(splittedLine[2]), (EClass)Enum.Parse(typeof(EClass), splittedLine[3])));
                }
                sr.Close();
            }
            Students = students;
        }
    }
}
