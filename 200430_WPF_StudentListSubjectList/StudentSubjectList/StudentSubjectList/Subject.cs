﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentSubjectList
{
    public enum EDifficulty
    {
        EASY,
        MIDDLE,
        HARD
    }
    class Subject
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public bool IsFavourite { get; set; }
        public EDifficulty Difficulty { get; set; }

        public Subject()
        {
            Code = "";
            Name = "";
            IsFavourite = false;
            Difficulty = EDifficulty.MIDDLE;
        }
        public Subject(string code, string name, bool isFavourite, EDifficulty difficulty)
        {
            Code = code;
            Name = name;
            IsFavourite = isFavourite;
            Difficulty = difficulty;
        }

        public string ToCSV()
        {
            return $"{Code};{Name};{IsFavourite};{Difficulty}";
        }
    }
}
