﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;

namespace StudentSubjectList
{
    class SubjectViewModel : INotifyPropertyChanged
    {
        private Subject subject;
        private string path = "subjects.csv";
        private ObservableCollection<Subject> subjects;
        private Subject selectedSubject;
        public event PropertyChangedEventHandler PropertyChanged;

        public Subject Subject
        {
            get
            {
                if (subject == null)
                    subject = new Subject();
                return subject;
            }
            set
            {
                subject = value;
                OnPropertyChanged("Subject");
            }
        }
        public EDifficulty Difficulty
        {
            get
            {
                return Subject.Difficulty;
            }
            set
            {
                Subject.Difficulty = value;
                OnPropertyChanged("Difficulty");
            }
        }
        public Subject SelectedSubject
        {
            get
            {
                return selectedSubject;
            }
            set
            {
                selectedSubject = value;
                OnPropertyChanged("SelectedSubject");
            }
        }
        public int SubjectCount
        {
            get
            {
                return subjects.Count;
            }
        }
        public string Name
        {
            get
            {
                return subject.Name;
            }
            set
            {
                subject.Name = value;
                OnPropertyChanged("Name");
            }
        }
        public string Code
        {
            get
            {
                return Subject.Code;
            }
            set
            {
                Subject.Code = value;
                OnPropertyChanged("Code");
            }
        }
        public bool IsFavourite
        {
            get
            {
                return Subject.IsFavourite;
            }
            set
            {
                Subject.IsFavourite = value;
                OnPropertyChanged("IsFavourite");
            }
        }
        public ObservableCollection<Subject> Subjects
        {
            get
            {
                return subjects;
            }
            set
            {
                subjects = value;
                OnPropertyChanged("Subjects");
                OnPropertyChanged("SubjectCount");
            }
        }
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public ICommand AddCommand { get; private set; }
        public ICommand LoadCommand { get; private set; }
        public ICommand DeleteCommand { get; private set; }

        public SubjectViewModel()
        {
            LoadCommand = new LoadCommandSubject(this);
            AddCommand = new AddCommandSubject(this);
            DeleteCommand = new DeleteCommand(this);
            LoadCommand.Execute(this);
        }

        public void Save(Subject s)
        {
            StreamWriter sw = new StreamWriter(path, true);
            sw.WriteLine(s.ToCSV());
            sw.Close();
            LoadCommand.Execute(this);
        }
        public void LoadFromCSV()
        {
            ObservableCollection<Subject> subjects = new ObservableCollection<Subject>();
            if (File.Exists(path))
            {
                StreamReader sr = new StreamReader(path);
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    string[] splittedLine = line.Split(';');
                    subjects.Add(new Subject(splittedLine[0], splittedLine[1], Convert.ToBoolean(splittedLine[2]), (EDifficulty)Enum.Parse(typeof(EDifficulty), splittedLine[3])));
                }
                sr.Close();
            }
            Subjects = subjects;
        }
        public void Delete(Subject s)
        {
            if (s != null)
            {
                subjects.Remove(s);
                StreamWriter sw = new StreamWriter(path);
                foreach (Subject item in subjects)
                {
                    sw.WriteLine(item.ToCSV());
                }
                sw.Close();
            }
            OnPropertyChanged("SubjectCount");
        }
    }
}
