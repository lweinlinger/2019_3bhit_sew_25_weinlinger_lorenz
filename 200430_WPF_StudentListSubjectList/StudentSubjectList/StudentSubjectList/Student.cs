﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentSubjectList
{
    public enum EClass { _1AHIT,_1BHIT, _2AHIT, _2BHIT, _3AHIT, _3BHIT, _4YHIT,_5HIT}
    public class Student
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public int Age { get; set; } = 0;
        public EClass Clazz { get; set; }

        public Student(string firstname, string lastname, int age, EClass clazz)
        {
            this.Firstname = firstname;
            this.Lastname = lastname;
            this.Age = age;
            this.Clazz = clazz;
        }

        public Student(){ }
        public string ToCSV()
        {
            return $"{this.Firstname};{this.Lastname};{this.Age};{this.Clazz}";
        }

    }
}
