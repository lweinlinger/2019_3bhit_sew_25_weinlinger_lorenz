﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace StudentSubjectList
{
    internal class AddCommandSubject : ICommand
    {
        private SubjectViewModel parent;

        public AddCommandSubject(SubjectViewModel subjectViewModel)
        {
            parent = subjectViewModel;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            if (parent.SubjectCount + 1 > 20)
                return false;
            else
                return true;
        }

        public void Execute(object parameter)
        {
            parent.Save(parent.Subject);
        }
    }
}
