﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace StudentSubjectList
{
    internal class DeleteCommand : ICommand
    {
        private SubjectViewModel parent;

        public DeleteCommand(SubjectViewModel subjectViewModel)
        {
            parent = subjectViewModel;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            parent.Delete(parent.SelectedSubject);
        }
    }
}
