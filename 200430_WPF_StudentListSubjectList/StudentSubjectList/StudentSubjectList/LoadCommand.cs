﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace StudentSubjectList
{
    class LoadCommand : ICommand
    {
        private StudentViewModel parent;
        public event EventHandler CanExecuteChanged;

        public LoadCommand(StudentViewModel studentViewModel)
        {
            parent = studentViewModel;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            parent.LoadFromCSV();
        }
    }
}
