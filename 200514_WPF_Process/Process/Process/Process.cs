﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.IO;

namespace Process
{
    public enum EState { Running, Ready, Blocked }
    class Processes
    {
        string description;
        int id;
        int importance;
        int progressPercent;
        EState state;
        bool scheduled;
        bool threadSafe;


        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }
        public int Importance
        {
            get
            {
                return importance;
            }
            set
            {
                importance = value;
            }
        }
        public int ProgressPercent
        {
            get
            {
                return progressPercent;
            }
            set
            {
                progressPercent = value;
            }
        }
        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }
        public EState State
        {
            get
            {
                return state;
            }
            set
            {
                state = value;
            }
        }
        public bool Scheduled
        {
            get
            {
                return scheduled;
            }
            set
            {
                scheduled = value;
            }
        }
        public bool ThreadSafe
        {
            get
            {
                return threadSafe;
            }
            set
            {
                threadSafe = value;
            }
        }

        public Processes(int id, int importance, bool scheduled, bool threadSafe, string description, int progressPercent, EState state)
        {
            this.id = id;
            this.importance = importance;
            this.scheduled = scheduled;
            this.threadSafe = threadSafe;
            this.description = description;
            this.progressPercent = progressPercent;
            this.state = state;
        }
        public Processes() { }

        public static void ToCSV(ObservableCollection<Processes> s)
        {
            StreamWriter sw = new StreamWriter("Processes.csv");
            foreach (Processes item in s)
                sw.WriteLine(Processes.ToCsv(item));
            sw.Close();
        }
        private static string ToCsv(Processes process)
        {
            return $"{process.Id};{process.Importance};{process.Scheduled};{process.ThreadSafe};{process.Description};{process.ProgressPercent};{process.State}";
        }
        public static ObservableCollection<Processes> Read()
        {
            ObservableCollection<Processes> processes = new ObservableCollection<Processes>();
            StreamReader sr = new StreamReader("Processes.csv");
            string line;
            string[] words;

            while ((line = sr.ReadLine()) != null)
            {
                words = line.Split(';');
                processes.Add(new Processes(Convert.ToInt32(words[0]), Convert.ToInt32(words[1]), Convert.ToBoolean(words[2]), Convert.ToBoolean(words[3]), words[4],
                    Convert.ToInt32(words[5]), (EState)Enum.Parse(typeof(EState), words[6])));
            }
            return processes;
        }
    }
}
