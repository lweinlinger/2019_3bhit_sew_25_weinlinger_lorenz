﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Windows.Input;

namespace Process
{
    class ProcessesVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string property)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
        public ObservableCollection<Processes> ProcessList { get; private set; }

        public ICommand AddCommand
            => new RelayCommand(
                    o =>
                    {
                        ProcessList.Add(CurrentProcess);
                        SelectedItem = null;
                        Processes.ToCSV(ProcessList);
                    }
                );
        public ICommand ClearCommand
            => new RelayCommand(
                    o =>
                    {
                        ProcessList.Clear();
                        File.Delete("process.csv");
                        File.Create("process.csv");
                    }
                );
        public ICommand DeleteCommand
            => new RelayCommand(
                    o =>
                    {
                        ProcessList.Remove(CurrentProcess);
                        Processes.ToCSV(ProcessList);
                    }
                );

        public Processes SelectedItem
        {
            get => ProcessList.Contains(CurrentProcess) ? CurrentProcess : null;
            set
            {
                CurrentProcess = value == null ? new Processes() : value;
            }
        }

        private Processes currentProcess = new Processes();
        public Processes CurrentProcess
        {
            get => this.currentProcess;
            set
            {
                this.currentProcess = value;
                OnPropertyChanged("CurrentProcess");
            }
        }

        public ProcessesVM() : this(Processes.Read()) { }
        public ProcessesVM(IEnumerable<Processes> processes)
        {
            this.ProcessList = (ObservableCollection<Processes>)processes;
        }
    }
}
