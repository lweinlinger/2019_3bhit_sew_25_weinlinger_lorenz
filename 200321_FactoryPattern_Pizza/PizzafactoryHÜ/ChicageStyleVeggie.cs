﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzafactoryHÜ
{
    class ChicageStyleVeggie : Pizza
    {
        public ChicageStyleVeggie()
        {
            name = "Chicago Style Veggie Pizza";
            dough = "Thin Crust Dough";
            sauce = "Marinara Sauce";

            toppings.Add("Veggies");
        }
    }
}
