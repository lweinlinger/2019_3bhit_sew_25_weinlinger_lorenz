﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzafactoryHÜ
{
    class Program
    {
        static void Main(string[] args)
        {
            PizzaStore myPS = new NYPizzaStore();
            Pizza pizza = myPS.orderPizza("pepperoni");

            Console.WriteLine("I ordered a " + pizza.GetName());

            myPS = new ChicagoPizzaStore();
            pizza = myPS.orderPizza("clam");
            Console.WriteLine("I ordered a " + pizza.GetName());
        }
    }
}
