﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzafactoryHÜ
{
    class ChicagoStyleClam : Pizza
    {
        public ChicagoStyleClam()
        {
            name = "Chicago Style Clam Pizza";
            dough = "Thin Crust Dough";
            sauce = "Marinara Sauce";

            toppings.Add("Clams");
        }
    }
}
