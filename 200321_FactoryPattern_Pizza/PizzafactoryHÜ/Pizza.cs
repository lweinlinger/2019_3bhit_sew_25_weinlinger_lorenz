﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzafactoryHÜ
{
    public abstract class Pizza
    {
        public string name;
        public string dough;
        public string sauce;

        public List<string> toppings = new List<string>();

        public void Prepare()
        {
            Console.WriteLine("Preparing " + name);
            Console.WriteLine("Adding toppings: ");
            foreach(string topping in toppings)
            {
                Console.WriteLine("   " + topping);
            }
        }

        public void Bake()
        {
            Console.WriteLine("Baking...");
        }

        public void Cut()
        {
            Console.WriteLine("Cutting the Pizza");
        }

        public void Box()
        {
            Console.WriteLine("The Pizza is put in the pizzabox");
        }

        public string GetName()
        {
            return name;
        }
    }
}
