﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzafactoryHÜ
{
    class ChicagoPizzaStore : PizzaStore
    {
        protected override Pizza createPizza(string type)
        {
            switch (type)
            {
                case "pepperoni":
                    return new ChicagoStylePepperoni();
                case "clam":
                    return new ChicagoStyleClam();
                case "veggie":
                    return new ChicageStyleVeggie();
                default:
                    return null;
            }
        }
    }
}
