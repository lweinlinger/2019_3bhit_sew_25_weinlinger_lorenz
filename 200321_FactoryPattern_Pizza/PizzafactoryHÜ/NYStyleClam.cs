﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzafactoryHÜ
{
    class NYStyleClam : Pizza
    {
        public NYStyleClam()
        {
            name = "NY Style Clam Pizza";
            dough = "Thin Crust Dough";
            sauce = "Marinara Sauce";

            toppings.Add("Sum good clams");
        }
    }
}
