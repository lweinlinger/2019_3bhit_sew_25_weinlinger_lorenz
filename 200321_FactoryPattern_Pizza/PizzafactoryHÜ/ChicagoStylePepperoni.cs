﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzafactoryHÜ
{
    class ChicagoStylePepperoni : Pizza
    {
        public ChicagoStylePepperoni()
        {
            name = "Chicago Style Pepperoni Pizza";
            dough = "Thin Crust Dough";
            sauce = "Marinara Sauce";

            toppings.Add("Pepperoni");
        }
    }
}
