﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HappyBirthdayCongratsEvent
{
    public delegate string MyDel(string str);
    class Person
    {
        event MyDel Sendwishes;

        public Person()
        {
            this.Sendwishes += new MyDel(this.Congratulate);
            //this.Sendwishes += new MyDel(this.SayGoodbye);
        }
        
        public string Congratulate(string nickname)
        {
            return "Happy Birthday " + nickname;
        }

        //public string SayGoodbye(string wishes)
        //{
        //    return wishes + "\nHave a wonderful day!";
        //}

        public static void Test()
        {
            Person p = new Person();
            string result = p.Sendwishes("Sweety!");
            Console.WriteLine(result);
        }
    }
}
