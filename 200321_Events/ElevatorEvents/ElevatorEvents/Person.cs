﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorEvents
{
    class Person
    {
        public string Name { get; }
        public int Weight { get; }

        public Person(string name, int weight)
        {
            Name = name;
            Weight = weight;
        }
    }
}
