﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorEvents
{
    class Program
    {
        static void Main(string[] args)
        {
            Elevator e = new Elevator("2009/12345", 500);
            for(int i = 0; i < 7; i++)
            {
                int weight = 50 + i * 10;
                string name = "Klaus" + i;
                Person p = new Person(name, weight);
                e.Boarding(p);
            }

        }
    }
}
