﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorEvents
{
    delegate void TooHeavyDelegate(Person p);
    class Elevator
    {
        event TooHeavyDelegate WeightLimitExceeded;
        string label;
        int maxWeight;
        List<Person> passengers = new List<Person>();

        public Elevator(string _label, int _maxWeight)
        {
            this.WeightLimitExceeded += new TooHeavyDelegate(this.AlarmTone);
            this.label = _label;
            this.maxWeight = _maxWeight;
        }

        public void Boarding(Person p)
        {
            passengers.Add(p);
            CheckWeight();
        }

        public void CheckWeight()
        {
            int weight = 0;

            foreach(Person p in passengers)
            {
                weight += p.Weight;
            }

            Console.WriteLine($"{passengers[passengers.Count - 1].Name} steigt ein. Gesamtgewicht: {weight}.");

            if (weight > maxWeight)
            {
                Console.WriteLine("Maximalgewicht überschritten.");
                WeightLimitExceeded(passengers[passengers.Count - 1]);
            }

        }

        public void AlarmTone(Person p)
        {
            Console.WriteLine($"{p.Name} steigt in den Aufzug und der Alarm wird aktiviert.");
            Unboarding(p);
        }
        public void Unboarding(Person p)
        {
            passengers.Remove(p);
            Console.WriteLine($"{p.Name} steigt aus dem Aufzug aus.");
        }

    }
}
