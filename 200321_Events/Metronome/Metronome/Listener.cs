﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metronome
{
   
    delegate void TickHandler(Metronome m, TimeOfTick e);
    class Listener
    {
        public void Subscribe(Metronome m)
        {
            m.Tick += new Metronome.TickHandler(HeardIt);
        }
        private void HeardIt(Metronome m, TimeOfTick e)
        {
            Console.WriteLine("HEARD IT AT "+e.Time);
        }
    }
}
