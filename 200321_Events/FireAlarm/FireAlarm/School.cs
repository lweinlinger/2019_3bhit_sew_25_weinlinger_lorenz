﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FireAlarm
{
    class School
    {
        public delegate void FireDelegate(string type);
        public event FireDelegate FireAlarm;

        public School() { }

        public int GetPlaceNumber(Clazz c)
        {
            switch (c.Name)
            {
                case "1AHIT":
                    return 1;
                case "1BHIT":
                    return 2;
                case "2AHIT":
                    return 3;
                case "2BHIT":
                    return 4;
                case "3AHIT":
                    return 5;
                case "3BHIT":
                    return 6;
                case "4YHIT":
                    return 7;
                case "5HIT":
                    return 8;
                default:
                    return 0;

            }
        }

        public void StartFireAlarm(string type)
        {
            FireAlarm(type);
        }
    }
}
