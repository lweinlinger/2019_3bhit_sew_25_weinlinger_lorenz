﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FireAlarm
{
    class Program
    {
        static void Main(string[] args)
        {
            School school = new School();
            Clazz clazz = new Clazz("3BHIT");

            Pupil s1 = new Pupil("Marius", clazz, school);
            Pupil s2 = new Pupil("Bernhard", clazz, school);
            Pupil s3 = new Pupil("Julia", clazz, school);

            clazz = new Clazz("2BHIT");
            s1 = new Pupil("Klaus", clazz, school);
            s2 = new Pupil("Norbert", clazz, school);

            school.StartFireAlarm("Coronaalarm");

        }
    }
}
