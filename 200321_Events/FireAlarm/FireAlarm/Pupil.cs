﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FireAlarm
{
    class Pupil
    {
        private string name;
        private Clazz clazz;
        private School school;

        public Pupil(string _name, Clazz _clazz, School _school)
        {
            name = _name;
            clazz = _clazz;
            school = _school;
            school.FireAlarm += new School.FireDelegate(EscapeRoute);
        }

        private void EscapeRoute(string type)
        {
            Console.WriteLine($"{type}: Schüler {name} begibt sich auf den Hof Nr.{school.GetPlaceNumber(clazz)}");
        }
    }
}
