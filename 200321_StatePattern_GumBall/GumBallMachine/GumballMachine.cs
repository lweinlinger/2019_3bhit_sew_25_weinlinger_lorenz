﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GumBallMachine
{
    class GumballMachine
    {
        public int gumBalls;
        IState state;

        public HasQuarter HasQuarter{get;}
        public NoQuarter NoQuarter { get; }
        public Sold Sold { get; }
        public SoldOut SoldOut { get; }
        public Winner Winner { get; }

        public GumballMachine(int _gumBalls)
        {
            gumBalls = _gumBalls;
            state = new NoQuarter(this);
            HasQuarter = new HasQuarter(this);
            NoQuarter = new NoQuarter(this);
            Sold = new Sold(this);
            SoldOut = new SoldOut(this);
            Winner = new Winner(this);
        }

        public void InsertQuarter()
        {
            state.InsertQuarter();
        }

        public void EjectQuarter()
        {
            state.EjectQuarter();
        }

        public void TurnCrank()
        {
            state.TurnCrank();
        }

        public void Dispense()
        {
            state.Dispense();
        }

        public void SetState(IState newState)
        {
            state = newState;
        }

        public void ReleaseBall()
        {
            Console.WriteLine("A Gumball rolls out of the Machine...");
            if (gumBalls > 0)
                gumBalls--;
        }
    }
}
