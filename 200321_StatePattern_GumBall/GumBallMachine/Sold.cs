﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GumBallMachine
{
    class Sold : IState
    {
        GumballMachine gumBallMachine;

        public Sold(GumballMachine gbm)
        {
            gumBallMachine = gbm;
        }

        public void Dispense()
        {
            gumBallMachine.ReleaseBall();
            if (gumBallMachine.gumBalls > 0)
                gumBallMachine.SetState(gumBallMachine.NoQuarter);
            else
                gumBallMachine.SetState(gumBallMachine.SoldOut);
        }

        public void EjectQuarter()
        {
            Console.WriteLine("You can't eject your Quarter anymore.");
        }

        public void InsertQuarter()
        {
            Console.WriteLine("You are already getting your Gumball.");
        }

        public void TurnCrank()
        {
            Console.WriteLine("You only get one Gumball.");
        }
    }
}
