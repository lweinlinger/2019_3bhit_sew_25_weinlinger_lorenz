﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GumBallMachine
{
    class Winner : IState
    {
        GumballMachine gumBallMachine;

        public Winner(GumballMachine gbm)
        {
            gumBallMachine = gbm;
        }
        public void Dispense()
        {
            Console.WriteLine("Congratulations! You are a winner and get two gumballs!");
            if (gumBallMachine.gumBalls > 0)
            {
                gumBallMachine.ReleaseBall();
                if (gumBallMachine.gumBalls > 0)
                {
                    gumBallMachine.ReleaseBall();
                    gumBallMachine.SetState(gumBallMachine.NoQuarter);
                }
                else
                {
                    Console.WriteLine("Out of gumballs :(");
                    gumBallMachine.SetState(gumBallMachine.SoldOut);
                }
            }                
            else
            {
                Console.WriteLine("Out of gumballs :(");
                gumBallMachine.SetState(gumBallMachine.SoldOut);
            }

            
        }

        public void EjectQuarter()
        {
            Console.WriteLine("There is no Quarter to be ejected.");
        }

        public void InsertQuarter()
        {
            Console.WriteLine("You are already getting your gumballs.");
        }

        public void TurnCrank()
        {
            Console.WriteLine("You cant turn the crank twice!");
        }
    }
}
