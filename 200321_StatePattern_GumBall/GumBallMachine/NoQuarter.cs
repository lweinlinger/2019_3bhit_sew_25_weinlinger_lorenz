﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GumBallMachine
{
    class NoQuarter : IState
    {
        GumballMachine gumBallMachine;
        public NoQuarter(GumballMachine gbm)
        {
            gumBallMachine = gbm;
        }

        public void Dispense()
        {
            Console.WriteLine("You first need to insert a Quarter.");
        }

        public void EjectQuarter()
        {
            Console.WriteLine("There is no Quarter to be Ejected");
        }

        public void InsertQuarter()
        {
            Console.WriteLine("You have inserted a Quarter.");
            gumBallMachine.SetState(gumBallMachine.HasQuarter);
        }

        public void TurnCrank()
        {
            Console.WriteLine("You first need to insert a Quarter.");
        }
    }
}
