﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GumBallMachine
{
    class Program
    {
        static void Main(string[] args)
        {
            GumballMachine gbm = new GumballMachine(5);
            gbm.InsertQuarter();
            gbm.EjectQuarter();
            gbm.TurnCrank();
            gbm.InsertQuarter();
            gbm.TurnCrank();
            gbm.InsertQuarter();
            gbm.TurnCrank();
            gbm.InsertQuarter();
            gbm.TurnCrank();
            gbm.InsertQuarter();
            gbm.TurnCrank();
            gbm.InsertQuarter();
            gbm.TurnCrank();

        }
    }
}
