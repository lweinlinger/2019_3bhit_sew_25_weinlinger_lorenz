﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GumBallMachine
{
    class SoldOut : IState
    {
        GumballMachine gumBallMachine;

        public SoldOut(GumballMachine gbm)
        {
            gumBallMachine = gbm;
        }

        public void Dispense()
        {
            Console.WriteLine("There are no gumballs left :(");
        }

        public void EjectQuarter()
        {
            Console.WriteLine("You get your Quarter back.");
            gumBallMachine.SetState(gumBallMachine.NoQuarter);
        }

        public void InsertQuarter()
        {
            Console.WriteLine("You can't insert two Quarters.");
        }

        public void TurnCrank()
        {
            Console.WriteLine("There are no gumballs left :(");
        }
    }
}
