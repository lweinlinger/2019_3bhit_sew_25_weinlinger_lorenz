﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GumBallMachine
{
    class HasQuarter : IState
    {
        GumballMachine gumBallMachine;
        Random randomWinner = new Random();
        public HasQuarter(GumballMachine gbm)
        {
            gumBallMachine = gbm;
        }
        public void Dispense()
        {
            Console.WriteLine("Please turn the crank first.");
        }

        public void EjectQuarter()
        {
            Console.WriteLine("Your Quarter has been ejected.");
            gumBallMachine.SetState(gumBallMachine.NoQuarter);
        }

        public void InsertQuarter()
        {
            Console.WriteLine("You can't insert two Quarters.");
        }

        public void TurnCrank()
        {
            Console.WriteLine("You turn the Crank");
            if (randomWinner.Next(2) == 1)
            {
                gumBallMachine.SetState(gumBallMachine.Winner);
                gumBallMachine.Dispense();
            }
            else
            {
                gumBallMachine.SetState(gumBallMachine.Sold);
                gumBallMachine.Dispense();
            }
        }
    }
}
