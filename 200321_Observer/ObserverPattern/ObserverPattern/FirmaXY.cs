﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPattern
{
    class FirmaXY : Subscriber
    {
        public void RecievePaper(Paper paper)
        {
            Console.WriteLine("FirmaXY erhält die aktuelle Zeitung: " + paper.Title);
        }
    }
}
