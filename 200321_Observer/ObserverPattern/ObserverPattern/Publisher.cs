﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPattern
{
    abstract class Publisher
    {
        private List<Subscriber> subscriberList = new List<Subscriber>();

        public void AddSubscriber(Subscriber subscriber)
        {
            subscriberList.Add(subscriber);
        }

        public void RemoveSubscriber(Subscriber subscriber)
        {
            subscriberList.Remove(subscriber);
        }

        protected void DistributePaper(Paper paper)
        {
            foreach(Subscriber sub in subscriberList)
            {
                sub.RecievePaper(paper);
            }
        }
    }
}
