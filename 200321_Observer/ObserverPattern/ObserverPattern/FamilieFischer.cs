﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPattern
{
    class FamilieFischer : Subscriber
    {
        public void RecievePaper(Paper paper)
        {
            Console.WriteLine("Familie Fischer erhält die aktuelle Zeitung: " + paper.Title);
        }
    }
}
