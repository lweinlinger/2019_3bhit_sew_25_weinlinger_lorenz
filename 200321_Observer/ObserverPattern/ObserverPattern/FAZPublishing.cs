﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPattern
{
    class FAZPublishing : Publisher
    {
        public Paper currentPaper { get; private set; }

        public void setCurrentPaper(Paper _currentPaper)
        {
            currentPaper = _currentPaper;
            DistributePaper(currentPaper);
        }

    }
}
