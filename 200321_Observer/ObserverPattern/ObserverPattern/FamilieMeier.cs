﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPattern
{
    class FamilieMeier : Subscriber
    {
        public void RecievePaper(Paper paper)
        {
            Console.WriteLine("Familie Meier erhält die aktuelle Zeitung: " + paper.Title);
        }
    }
}
