﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPattern
{
    class Paper
    {
        public string Title { get; private set; }

        public Paper(string title)
        {
            Title = title;
        }

    }
}
