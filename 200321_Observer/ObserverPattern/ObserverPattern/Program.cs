﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            FAZPublishing publisher = new FAZPublishing();
            publisher.AddSubscriber(new FamilieFischer());
            publisher.AddSubscriber(new FamilieMeier());
            FirmaXY firm = new FirmaXY();
            publisher.AddSubscriber(firm);

            publisher.setCurrentPaper(new Paper("Skandal!"));

            publisher.RemoveSubscriber(firm);
            publisher.setCurrentPaper(new Paper("Doch alles halb so wild!"));
        }
    }
}
